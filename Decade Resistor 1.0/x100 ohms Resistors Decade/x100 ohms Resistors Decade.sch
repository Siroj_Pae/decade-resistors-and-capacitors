EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:x100 ohms Resistors Decade-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X09 J1
U 1 1 5B38E483
P 5450 2700
F 0 "J1" H 5450 3200 50  0000 C CNN
F 1 "CONN_02X09" V 5450 2700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09_Pitch2.54mm" H 5450 1500 50  0001 C CNN
F 3 "" H 5450 1500 50  0001 C CNN
F 4 "PHDS80G11/RH" H 5450 2700 60  0001 C CNN "partno"
	1    5450 2700
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 5B38E484
P 5050 3250
F 0 "R1" V 5130 3250 50  0000 C CNN
F 1 "100" V 5050 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4980 3250 50  0001 C CNN
F 3 "" H 5050 3250 50  0001 C CNN
F 4 "WR06X1000FTL" H 5050 3250 60  0001 C CNN "partno"
	1    5050 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2450 5000 2450
Wire Wire Line
	5000 2450 5000 3400
Wire Wire Line
	5000 3400 5050 3400
Wire Wire Line
	5050 2950 5050 3100
Wire Wire Line
	5150 2450 5100 2450
Wire Wire Line
	5100 2450 5100 3400
Wire Wire Line
	5100 3400 5150 3400
Wire Wire Line
	5150 2950 5150 3100
Wire Wire Line
	5250 2450 5200 2450
Wire Wire Line
	5200 2450 5200 3400
Wire Wire Line
	5200 3400 5250 3400
Wire Wire Line
	5250 3100 5250 2950
Wire Wire Line
	5350 2450 5300 2450
Wire Wire Line
	5300 2450 5300 3400
Wire Wire Line
	5300 3400 5350 3400
Wire Wire Line
	5350 3100 5350 2950
Wire Wire Line
	5450 2450 5400 2450
Wire Wire Line
	5400 2450 5400 3400
Wire Wire Line
	5400 3400 5450 3400
Wire Wire Line
	5450 3100 5450 2950
Wire Wire Line
	5550 2450 5500 2450
Wire Wire Line
	5500 2450 5500 3400
Wire Wire Line
	5500 3400 5550 3400
Wire Wire Line
	5550 3100 5550 2950
Wire Wire Line
	5650 2450 5600 2450
Wire Wire Line
	5600 2450 5600 3400
Wire Wire Line
	5600 3400 5650 3400
Wire Wire Line
	5650 3100 5650 2950
Wire Wire Line
	5750 2450 5700 2450
Wire Wire Line
	5700 2450 5700 3400
Wire Wire Line
	5700 3400 5750 3400
Wire Wire Line
	5750 3100 5750 2950
Wire Wire Line
	5850 2450 5800 2450
Wire Wire Line
	5800 3400 5850 3400
Wire Wire Line
	5850 3100 5850 2950
Wire Wire Line
	5800 2450 5800 3400
$Comp
L R R2
U 1 1 5B386600
P 5150 3250
F 0 "R2" V 5230 3250 50  0000 C CNN
F 1 "100" V 5150 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5080 3250 50  0001 C CNN
F 3 "" H 5150 3250 50  0001 C CNN
F 4 "WR06X1000FTL" H 5150 3250 60  0001 C CNN "partno"
	1    5150 3250
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5B38661F
P 5250 3250
F 0 "R3" V 5330 3250 50  0000 C CNN
F 1 "100" V 5250 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5180 3250 50  0001 C CNN
F 3 "" H 5250 3250 50  0001 C CNN
F 4 "WR06X1000FTL" H 5250 3250 60  0001 C CNN "partno"
	1    5250 3250
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5B38663D
P 5350 3250
F 0 "R4" V 5430 3250 50  0000 C CNN
F 1 "100" V 5350 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5280 3250 50  0001 C CNN
F 3 "" H 5350 3250 50  0001 C CNN
F 4 "WR06X1000FTL" H 5350 3250 60  0001 C CNN "partno"
	1    5350 3250
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 5B38665E
P 5450 3250
F 0 "R5" V 5530 3250 50  0000 C CNN
F 1 "100" V 5450 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5380 3250 50  0001 C CNN
F 3 "" H 5450 3250 50  0001 C CNN
F 4 "WR06X1000FTL" H 5450 3250 60  0001 C CNN "partno"
	1    5450 3250
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5B386682
P 5550 3250
F 0 "R6" V 5630 3250 50  0000 C CNN
F 1 "100" V 5550 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5480 3250 50  0001 C CNN
F 3 "" H 5550 3250 50  0001 C CNN
F 4 "WR06X1000FTL" H 5550 3250 60  0001 C CNN "partno"
	1    5550 3250
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 5B3866A9
P 5650 3250
F 0 "R7" V 5730 3250 50  0000 C CNN
F 1 "100" V 5650 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5580 3250 50  0001 C CNN
F 3 "" H 5650 3250 50  0001 C CNN
F 4 "WR06X1000FTL" H 5650 3250 60  0001 C CNN "partno"
	1    5650 3250
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 5B3866D3
P 5750 3250
F 0 "R8" V 5830 3250 50  0000 C CNN
F 1 "100" V 5750 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5680 3250 50  0001 C CNN
F 3 "" H 5750 3250 50  0001 C CNN
F 4 "WR06X1000FTL" H 5750 3250 60  0001 C CNN "partno"
	1    5750 3250
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 5B386700
P 5850 3250
F 0 "R9" V 5930 3250 50  0000 C CNN
F 1 "100" V 5850 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 5780 3250 50  0001 C CNN
F 3 "" H 5850 3250 50  0001 C CNN
F 4 "WR06X1000FTL" H 5850 3250 60  0001 C CNN "partno"
	1    5850 3250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
