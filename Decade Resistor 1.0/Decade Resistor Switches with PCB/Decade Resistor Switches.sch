EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_shieldsNCL
LIBS:auv
LIBS:HG_74xx
LIBS:HG_BMS
LIBS:HG_device
LIBS:HG_regulators
LIBS:mylib
LIBS:Decade Resistor Switches-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Rotary_Switch_12pos SW1
U 1 1 5B36FB82
P 3000 4600
F 0 "SW1" H 3000 4700 60  0000 C CNN
F 1 "Rotary_Switch_12pos" H 3000 4250 60  0000 C CNN
F 2 "myfootprint:Rotary_SW_noname" H 1950 5000 60  0001 C CNN
F 3 "" H 1950 5000 60  0001 C CNN
	1    3000 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 5100 3150 5100
Wire Wire Line
	2050 5100 2050 3700
Wire Wire Line
	2050 3700 2550 3700
Wire Wire Line
	2500 4750 2100 4750
Wire Wire Line
	2100 4750 2100 3200
Wire Wire Line
	2100 3200 2600 3200
Wire Wire Line
	2600 3200 2600 3700
Wire Wire Line
	2600 3700 2650 3700
Connection ~ 2550 3200
Wire Wire Line
	2500 4600 2150 4600
Wire Wire Line
	2150 4600 2150 3150
Wire Wire Line
	2150 3150 2700 3150
Wire Wire Line
	2650 3150 2650 3200
Wire Wire Line
	2700 3150 2700 3700
Wire Wire Line
	2700 3700 2750 3700
Connection ~ 2650 3150
Wire Wire Line
	2500 4450 2200 4450
Wire Wire Line
	2200 4450 2200 3100
Wire Wire Line
	2200 3100 2800 3100
Wire Wire Line
	2750 3100 2750 3200
Wire Wire Line
	2800 3100 2800 3700
Wire Wire Line
	2800 3700 2850 3700
Connection ~ 2750 3100
Wire Wire Line
	2850 4100 2250 4100
Wire Wire Line
	2250 4100 2250 3050
Wire Wire Line
	2250 3050 2900 3050
Wire Wire Line
	2850 3050 2850 3200
Wire Wire Line
	2900 3050 2900 3700
Wire Wire Line
	2900 3700 2950 3700
Connection ~ 2850 3050
Wire Wire Line
	3000 4000 3000 4100
Wire Wire Line
	3000 4000 2300 4000
Wire Wire Line
	2300 4000 2300 3000
Wire Wire Line
	2300 3000 3000 3000
Wire Wire Line
	2950 3000 2950 3200
Wire Wire Line
	3000 3000 3000 3700
Wire Wire Line
	3000 3700 3050 3700
Connection ~ 2950 3000
Wire Wire Line
	3150 4100 3150 4000
Wire Wire Line
	3150 4000 3700 4000
Wire Wire Line
	3700 4000 3700 3000
Wire Wire Line
	3700 3000 3050 3000
Wire Wire Line
	3050 3000 3050 3200
Wire Wire Line
	3100 3000 3100 3700
Wire Wire Line
	3100 3700 3150 3700
Connection ~ 3100 3000
Wire Wire Line
	3750 3050 3750 4450
Wire Wire Line
	3750 3050 3150 3050
Wire Wire Line
	3150 3050 3150 3200
Wire Wire Line
	3200 3050 3200 3700
Wire Wire Line
	3200 3700 3250 3700
Connection ~ 3200 3050
Wire Wire Line
	3500 4600 3800 4600
Wire Wire Line
	3800 4600 3800 3100
Wire Wire Line
	3800 3100 3250 3100
Wire Wire Line
	3250 3100 3250 3200
Wire Wire Line
	3300 3100 3300 3700
Wire Wire Line
	3300 3700 3350 3700
Connection ~ 3300 3100
Wire Wire Line
	3750 4450 3500 4450
Wire Wire Line
	3500 4750 3850 4750
Wire Wire Line
	3850 4750 3850 3150
Wire Wire Line
	3850 3150 3350 3150
Wire Wire Line
	3350 3150 3350 3200
$Comp
L CONN_02X09 J3
U 1 1 5B3719DC
P 2950 3450
F 0 "J3" H 2950 3950 50  0000 C CNN
F 1 "CONN_02X09" V 2950 3450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09_Pitch2.54mm" H 2950 2250 50  0001 C CNN
F 3 "" H 2950 2250 50  0001 C CNN
F 4 "FHDS20G11/RH" H 2950 3450 60  0001 C CNN "partno"
	1    2950 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2850 5100 2850 5250
Wire Wire Line
	2850 5250 3000 5250
Wire Wire Line
	3050 4850 1850 4850
Text GLabel 1850 4850 0    60   Input ~ 0
Termination1
Text GLabel 3000 5250 2    60   Input ~ 0
SW1
$Comp
L Rotary_Switch_12pos SW2
U 1 1 5B371C6D
P 5350 4600
F 0 "SW2" H 5350 4700 60  0000 C CNN
F 1 "Rotary_Switch_12pos" H 5350 4250 60  0000 C CNN
F 2 "myfootprint:Rotary_SW_noname" H 4300 5000 60  0001 C CNN
F 3 "" H 4300 5000 60  0001 C CNN
	1    5350 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5100 5500 5100
Wire Wire Line
	4400 5100 4400 3700
Wire Wire Line
	4400 3700 4900 3700
Wire Wire Line
	4850 4750 4450 4750
Wire Wire Line
	4450 4750 4450 3200
Wire Wire Line
	4450 3200 4950 3200
Wire Wire Line
	4950 3200 4950 3700
Wire Wire Line
	4950 3700 5000 3700
Connection ~ 4900 3200
Wire Wire Line
	4850 4600 4500 4600
Wire Wire Line
	4500 4600 4500 3150
Wire Wire Line
	4500 3150 5050 3150
Wire Wire Line
	5000 3150 5000 3200
Wire Wire Line
	5050 3150 5050 3700
Wire Wire Line
	5050 3700 5100 3700
Connection ~ 5000 3150
Wire Wire Line
	4850 4450 4550 4450
Wire Wire Line
	4550 4450 4550 3100
Wire Wire Line
	4550 3100 5150 3100
Wire Wire Line
	5100 3100 5100 3200
Wire Wire Line
	5150 3100 5150 3700
Wire Wire Line
	5150 3700 5200 3700
Connection ~ 5100 3100
Wire Wire Line
	5200 4100 4600 4100
Wire Wire Line
	4600 4100 4600 3050
Wire Wire Line
	4600 3050 5250 3050
Wire Wire Line
	5200 3050 5200 3200
Wire Wire Line
	5250 3050 5250 3700
Wire Wire Line
	5250 3700 5300 3700
Connection ~ 5200 3050
Wire Wire Line
	5350 4000 5350 4100
Wire Wire Line
	5350 4000 4650 4000
Wire Wire Line
	4650 4000 4650 3000
Wire Wire Line
	4650 3000 5350 3000
Wire Wire Line
	5300 3000 5300 3200
Wire Wire Line
	5350 3000 5350 3700
Wire Wire Line
	5350 3700 5400 3700
Connection ~ 5300 3000
Wire Wire Line
	5500 4100 5500 4000
Wire Wire Line
	5500 4000 6050 4000
Wire Wire Line
	6050 4000 6050 3000
Wire Wire Line
	6050 3000 5400 3000
Wire Wire Line
	5400 3000 5400 3200
Wire Wire Line
	5450 3000 5450 3700
Wire Wire Line
	5450 3700 5500 3700
Connection ~ 5450 3000
Wire Wire Line
	6100 3050 6100 4450
Wire Wire Line
	6100 3050 5500 3050
Wire Wire Line
	5500 3050 5500 3200
Wire Wire Line
	5550 3050 5550 3700
Wire Wire Line
	5550 3700 5600 3700
Connection ~ 5550 3050
Wire Wire Line
	5850 4600 6150 4600
Wire Wire Line
	6150 4600 6150 3100
Wire Wire Line
	6150 3100 5600 3100
Wire Wire Line
	5600 3100 5600 3200
Wire Wire Line
	5650 3100 5650 3700
Wire Wire Line
	5650 3700 5700 3700
Connection ~ 5650 3100
Wire Wire Line
	6100 4450 5850 4450
Wire Wire Line
	5850 4750 6200 4750
Wire Wire Line
	6200 4750 6200 3150
Wire Wire Line
	6200 3150 5700 3150
Wire Wire Line
	5700 3150 5700 3200
$Comp
L CONN_02X09 J4
U 1 1 5B371CB3
P 5300 3450
F 0 "J4" H 5300 3950 50  0000 C CNN
F 1 "CONN_02X09" V 5300 3450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09_Pitch2.54mm" H 5300 2250 50  0001 C CNN
F 3 "" H 5300 2250 50  0001 C CNN
F 4 "FHDS20G11/RH" H 5300 3450 60  0001 C CNN "partno"
	1    5300 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5200 5100 5200 5250
Wire Wire Line
	5200 5250 5350 5250
Wire Wire Line
	5400 4850 4200 4850
Text GLabel 4200 4850 0    60   Input ~ 0
SW1
Text GLabel 5350 5250 2    60   Input ~ 0
SW2
$Comp
L Rotary_Switch_12pos SW3
U 1 1 5B371D30
P 7700 4600
F 0 "SW3" H 7700 4700 60  0000 C CNN
F 1 "Rotary_Switch_12pos" H 7700 4250 60  0000 C CNN
F 2 "myfootprint:Rotary_SW_noname" H 6650 5000 60  0001 C CNN
F 3 "" H 6650 5000 60  0001 C CNN
	1    7700 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 5100 7850 5100
Wire Wire Line
	6750 5100 6750 3700
Wire Wire Line
	6750 3700 7250 3700
Wire Wire Line
	7200 4750 6800 4750
Wire Wire Line
	6800 4750 6800 3200
Wire Wire Line
	6800 3200 7300 3200
Wire Wire Line
	7300 3200 7300 3700
Wire Wire Line
	7300 3700 7350 3700
Connection ~ 7250 3200
Wire Wire Line
	7200 4600 6850 4600
Wire Wire Line
	6850 4600 6850 3150
Wire Wire Line
	6850 3150 7400 3150
Wire Wire Line
	7350 3150 7350 3200
Wire Wire Line
	7400 3150 7400 3700
Wire Wire Line
	7400 3700 7450 3700
Connection ~ 7350 3150
Wire Wire Line
	7200 4450 6900 4450
Wire Wire Line
	6900 4450 6900 3100
Wire Wire Line
	6900 3100 7500 3100
Wire Wire Line
	7450 3100 7450 3200
Wire Wire Line
	7500 3100 7500 3700
Wire Wire Line
	7500 3700 7550 3700
Connection ~ 7450 3100
Wire Wire Line
	7550 4100 6950 4100
Wire Wire Line
	6950 4100 6950 3050
Wire Wire Line
	6950 3050 7600 3050
Wire Wire Line
	7550 3050 7550 3200
Wire Wire Line
	7600 3050 7600 3700
Wire Wire Line
	7600 3700 7650 3700
Connection ~ 7550 3050
Wire Wire Line
	7700 4000 7700 4100
Wire Wire Line
	7700 4000 7000 4000
Wire Wire Line
	7000 4000 7000 3000
Wire Wire Line
	7000 3000 7700 3000
Wire Wire Line
	7650 3000 7650 3200
Wire Wire Line
	7700 3000 7700 3700
Wire Wire Line
	7700 3700 7750 3700
Connection ~ 7650 3000
Wire Wire Line
	7850 4100 7850 4000
Wire Wire Line
	7850 4000 8400 4000
Wire Wire Line
	8400 4000 8400 3000
Wire Wire Line
	8400 3000 7750 3000
Wire Wire Line
	7750 3000 7750 3200
Wire Wire Line
	7800 3000 7800 3700
Wire Wire Line
	7800 3700 7850 3700
Connection ~ 7800 3000
Wire Wire Line
	8450 3050 8450 4450
Wire Wire Line
	8450 3050 7850 3050
Wire Wire Line
	7850 3050 7850 3200
Wire Wire Line
	7900 3050 7900 3700
Wire Wire Line
	7900 3700 7950 3700
Connection ~ 7900 3050
Wire Wire Line
	8200 4600 8500 4600
Wire Wire Line
	8500 4600 8500 3100
Wire Wire Line
	8500 3100 7950 3100
Wire Wire Line
	7950 3100 7950 3200
Wire Wire Line
	8000 3100 8000 3700
Wire Wire Line
	8000 3700 8050 3700
Connection ~ 8000 3100
Wire Wire Line
	8450 4450 8200 4450
Wire Wire Line
	8200 4750 8550 4750
Wire Wire Line
	8550 4750 8550 3150
Wire Wire Line
	8550 3150 8050 3150
Wire Wire Line
	8050 3150 8050 3200
$Comp
L CONN_02X09 J5
U 1 1 5B371D76
P 7650 3450
F 0 "J5" H 7650 3950 50  0000 C CNN
F 1 "CONN_02X09" V 7650 3450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09_Pitch2.54mm" H 7650 2250 50  0001 C CNN
F 3 "" H 7650 2250 50  0001 C CNN
F 4 "FHDS20G11/RH" H 7650 3450 60  0001 C CNN "partno"
	1    7650 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7550 5100 7550 5250
Wire Wire Line
	7550 5250 7700 5250
Wire Wire Line
	7750 4850 6550 4850
Text GLabel 6550 4850 0    60   Input ~ 0
SW2
Text GLabel 7700 5250 2    60   Input ~ 0
SW3
$Comp
L Rotary_Switch_12pos SW4
U 1 1 5B371FFD
P 10100 4600
F 0 "SW4" H 10100 4700 60  0000 C CNN
F 1 "Rotary_Switch_12pos" H 10100 4250 60  0000 C CNN
F 2 "myfootprint:Rotary_SW_noname" H 9050 5000 60  0001 C CNN
F 3 "" H 9050 5000 60  0001 C CNN
	1    10100 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 5100 10250 5100
Wire Wire Line
	9150 5100 9150 3700
Wire Wire Line
	9150 3700 9650 3700
Wire Wire Line
	9600 4750 9200 4750
Wire Wire Line
	9200 4750 9200 3200
Wire Wire Line
	9200 3200 9700 3200
Wire Wire Line
	9700 3200 9700 3700
Wire Wire Line
	9700 3700 9750 3700
Connection ~ 9650 3200
Wire Wire Line
	9600 4600 9250 4600
Wire Wire Line
	9250 4600 9250 3150
Wire Wire Line
	9250 3150 9800 3150
Wire Wire Line
	9750 3150 9750 3200
Wire Wire Line
	9800 3150 9800 3700
Wire Wire Line
	9800 3700 9850 3700
Connection ~ 9750 3150
Wire Wire Line
	9600 4450 9300 4450
Wire Wire Line
	9300 4450 9300 3100
Wire Wire Line
	9300 3100 9900 3100
Wire Wire Line
	9850 3100 9850 3200
Wire Wire Line
	9900 3100 9900 3700
Wire Wire Line
	9900 3700 9950 3700
Connection ~ 9850 3100
Wire Wire Line
	9950 4100 9350 4100
Wire Wire Line
	9350 4100 9350 3050
Wire Wire Line
	9350 3050 10000 3050
Wire Wire Line
	9950 3050 9950 3200
Wire Wire Line
	10000 3050 10000 3700
Wire Wire Line
	10000 3700 10050 3700
Connection ~ 9950 3050
Wire Wire Line
	10100 4000 10100 4100
Wire Wire Line
	10100 4000 9400 4000
Wire Wire Line
	9400 4000 9400 3000
Wire Wire Line
	9400 3000 10100 3000
Wire Wire Line
	10050 3000 10050 3200
Wire Wire Line
	10100 3000 10100 3700
Wire Wire Line
	10100 3700 10150 3700
Connection ~ 10050 3000
Wire Wire Line
	10250 4100 10250 4000
Wire Wire Line
	10250 4000 10800 4000
Wire Wire Line
	10800 4000 10800 3000
Wire Wire Line
	10800 3000 10150 3000
Wire Wire Line
	10150 3000 10150 3200
Wire Wire Line
	10200 3000 10200 3700
Wire Wire Line
	10200 3700 10250 3700
Connection ~ 10200 3000
Wire Wire Line
	10850 3050 10850 4450
Wire Wire Line
	10850 3050 10250 3050
Wire Wire Line
	10250 3050 10250 3200
Wire Wire Line
	10300 3050 10300 3700
Wire Wire Line
	10300 3700 10350 3700
Connection ~ 10300 3050
Wire Wire Line
	10600 4600 10900 4600
Wire Wire Line
	10900 4600 10900 3100
Wire Wire Line
	10900 3100 10350 3100
Wire Wire Line
	10350 3100 10350 3200
Wire Wire Line
	10400 3100 10400 3700
Wire Wire Line
	10400 3700 10450 3700
Connection ~ 10400 3100
Wire Wire Line
	10850 4450 10600 4450
Wire Wire Line
	10600 4750 10950 4750
Wire Wire Line
	10950 4750 10950 3150
Wire Wire Line
	10950 3150 10450 3150
Wire Wire Line
	10450 3150 10450 3200
$Comp
L CONN_02X09 J6
U 1 1 5B372043
P 10050 3450
F 0 "J6" H 10050 3950 50  0000 C CNN
F 1 "CONN_02X09" V 10050 3450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09_Pitch2.54mm" H 10050 2250 50  0001 C CNN
F 3 "" H 10050 2250 50  0001 C CNN
F 4 "FHDS20G11/RH" H 10050 3450 60  0001 C CNN "partno"
	1    10050 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9950 5100 9950 5250
Wire Wire Line
	9950 5250 10100 5250
Wire Wire Line
	10150 4850 8950 4850
Text GLabel 8950 4850 0    60   Input ~ 0
SW3
Text GLabel 10100 5250 2    60   Input ~ 0
Termination2
Text Label 2250 4750 0    60   ~ 0
sw1-1
Text Label 2250 4600 0    60   ~ 0
sw1-2
Text Label 2250 4450 0    60   ~ 0
sw1-3
Text Label 2600 4100 0    60   ~ 0
sw1-4
Text Label 2700 4000 0    60   ~ 0
sw1-5
Text Label 3200 4000 0    60   ~ 0
sw1-6
Text Label 3550 4450 0    60   ~ 0
sw1-7
Text Label 3550 4600 0    60   ~ 0
sw1-8
Text Label 3550 4750 0    60   ~ 0
sw1-9
Text Label 4600 4750 0    60   ~ 0
sw2-1
Text Label 4600 4600 0    60   ~ 0
sw2-2
Text Label 4600 4450 0    60   ~ 0
sw2-3
Text Label 4800 4100 0    60   ~ 0
sw2-4
Text Label 4850 4000 0    60   ~ 0
sw2-5
Text Label 5600 4000 0    60   ~ 0
sw2-6
Text Label 5900 4450 0    60   ~ 0
sw2-7
Text Label 5900 4600 0    60   ~ 0
sw2-8
Text Label 5900 4750 0    60   ~ 0
sw2-9
Text Label 7000 4750 0    60   ~ 0
sw3-1
Text Label 7000 4600 0    60   ~ 0
sw3-2
Text Label 7000 4450 0    60   ~ 0
sw3-3
Text Label 7200 4100 0    60   ~ 0
sw3-4
Text Label 7250 4000 0    60   ~ 0
sw3-5
Text Label 7900 4000 0    60   ~ 0
sw3-6
Text Label 8250 4450 0    60   ~ 0
sw3-7
Text Label 8250 4600 0    60   ~ 0
sw3-8
Text Label 8250 4750 0    60   ~ 0
sw3-9
Text Label 9350 4750 0    60   ~ 0
sw4-1
Text Label 9350 4600 0    60   ~ 0
sw4-2
Text Label 9350 4450 0    60   ~ 0
sw4-3
Text Label 9550 4100 0    60   ~ 0
sw4-4
Text Label 9650 4000 0    60   ~ 0
sw4-5
Text Label 10350 4000 0    60   ~ 0
sw4-6
Text Label 10650 4450 0    60   ~ 0
sw4-7
Text Label 10650 4600 0    60   ~ 0
sw4-8
Text Label 10650 4750 0    60   ~ 0
sw4-9
$Comp
L CONN_01X02 J1
U 1 1 5B37576D
P 1800 2700
F 0 "J1" H 1800 2850 50  0000 C CNN
F 1 "CONN_01X02" V 1900 2700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x01_Pitch2.54mm" H 1800 2700 50  0001 C CNN
F 3 "" H 1800 2700 50  0001 C CNN
F 4 "PHSR40G10" H 1800 2700 60  0001 C CNN "partno"
	1    1800 2700
	1    0    0    -1  
$EndComp
Text GLabel 1450 2600 0    60   Input ~ 0
Termination1
Text GLabel 1450 2800 0    60   Input ~ 0
Termination2
Wire Wire Line
	1450 2600 1500 2600
Wire Wire Line
	1500 2600 1500 2650
Wire Wire Line
	1500 2650 1600 2650
Wire Wire Line
	1450 2800 1500 2800
Wire Wire Line
	1500 2750 1500 3050
Wire Wire Line
	1500 2750 1600 2750
$Comp
L CONN_01X02 J2
U 1 1 5B3EFB55
P 1800 3000
F 0 "J2" H 1800 3150 50  0000 C CNN
F 1 "CONN_01X02" V 1900 3000 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 1800 3000 50  0001 C CNN
F 3 "" H 1800 3000 50  0001 C CNN
F 4 "DG126-5.0-02P-14-19AH" H 1800 3000 60  0001 C CNN "partno"
	1    1800 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 2650 1550 2950
Wire Wire Line
	1550 2950 1600 2950
Connection ~ 1550 2650
Wire Wire Line
	1500 3050 1600 3050
Connection ~ 1500 2800
NoConn ~ 1550 2750
Connection ~ 2850 5100
Connection ~ 3000 5100
Connection ~ 5200 5100
Connection ~ 5350 5100
Connection ~ 7550 5100
Connection ~ 7700 5100
Connection ~ 9950 5100
Connection ~ 10100 5100
$EndSCHEMATC
