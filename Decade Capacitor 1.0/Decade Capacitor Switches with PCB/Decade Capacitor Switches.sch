EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_shieldsNCL
LIBS:auv
LIBS:HG_74xx
LIBS:HG_BMS
LIBS:HG_device
LIBS:HG_regulators
LIBS:mylib
LIBS:Decade Capacitor Switches-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Rotary_Switch_12pos SW1
U 1 1 5B3770AA
P 3350 4500
F 0 "SW1" H 3350 4600 60  0000 C CNN
F 1 "Rotary_Switch_12pos" H 3350 4150 60  0000 C CNN
F 2 "myfootprint:Rotary_SW_noname" H 2300 4900 60  0001 C CNN
F 3 "" H 2300 4900 60  0001 C CNN
	1    3350 4500
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X09 J3
U 1 1 5B377160
P 3350 3400
F 0 "J3" H 3350 3900 50  0000 C CNN
F 1 "CONN_02X09" V 3350 3400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09_Pitch2.54mm" H 3350 2200 50  0001 C CNN
F 3 "" H 3350 2200 50  0001 C CNN
F 4 "FHDS20G11/RH" H 3350 3400 60  0001 C CNN "partno"
	1    3350 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3050 3700 3050 3650
Wire Wire Line
	3150 3750 3150 3650
Wire Wire Line
	3200 4000 3200 3800
Wire Wire Line
	3200 3800 3250 3800
Wire Wire Line
	3250 3800 3250 3650
Wire Wire Line
	3350 4000 3350 3650
Wire Wire Line
	3500 3800 3450 3800
Wire Wire Line
	3450 3800 3450 3650
Wire Wire Line
	3500 3800 3500 4000
Wire Wire Line
	3850 4350 3850 3750
Wire Wire Line
	3850 3750 3550 3750
Wire Wire Line
	3550 3750 3550 3650
Wire Wire Line
	3150 3750 2850 3750
Wire Wire Line
	2850 3750 2850 4350
Wire Wire Line
	2850 4500 2800 4500
Wire Wire Line
	2800 4500 2800 3700
Wire Wire Line
	2800 3700 3050 3700
Wire Wire Line
	2850 4650 2750 4650
Wire Wire Line
	2750 4650 2750 3650
Wire Wire Line
	2750 3650 2950 3650
Wire Wire Line
	3650 3650 3650 3700
Wire Wire Line
	3650 3700 3900 3700
Wire Wire Line
	3900 3700 3900 4500
Wire Wire Line
	3900 4500 3850 4500
Wire Wire Line
	3850 4650 3950 4650
Wire Wire Line
	3950 4650 3950 3650
Wire Wire Line
	3950 3650 3750 3650
Wire Wire Line
	2950 3150 2950 3100
Wire Wire Line
	2950 3100 4050 3100
Wire Wire Line
	3750 3100 3750 3150
Wire Wire Line
	3050 3150 3050 3100
Connection ~ 3050 3100
Wire Wire Line
	3150 3100 3150 3150
Connection ~ 3150 3100
Wire Wire Line
	3250 3150 3250 3100
Connection ~ 3250 3100
Wire Wire Line
	3350 3100 3350 3150
Connection ~ 3350 3100
Wire Wire Line
	3450 3150 3450 3100
Connection ~ 3450 3100
Wire Wire Line
	3550 3150 3550 3100
Connection ~ 3550 3100
Wire Wire Line
	3650 3150 3650 3100
Connection ~ 3650 3100
Wire Wire Line
	4050 3100 4050 5250
Connection ~ 3750 3100
Wire Wire Line
	3400 4750 2650 4750
Text GLabel 4050 5250 2    60   Input ~ 0
Negative
Text GLabel 1700 3600 0    60   Input ~ 0
Positive
$Comp
L Rotary_Switch_12pos SW2
U 1 1 5B37799A
P 5200 4500
F 0 "SW2" H 5200 4600 60  0000 C CNN
F 1 "Rotary_Switch_12pos" H 5200 4150 60  0000 C CNN
F 2 "myfootprint:Rotary_SW_noname" H 4150 4900 60  0001 C CNN
F 3 "" H 4150 4900 60  0001 C CNN
	1    5200 4500
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X09 J4
U 1 1 5B3779A0
P 5200 3400
F 0 "J4" H 5200 3900 50  0000 C CNN
F 1 "CONN_02X09" V 5200 3400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09_Pitch2.54mm" H 5200 2200 50  0001 C CNN
F 3 "" H 5200 2200 50  0001 C CNN
F 4 "FHDS20G11/RH" H 5200 3400 60  0001 C CNN "partno"
	1    5200 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4900 3700 4900 3650
Wire Wire Line
	5000 3750 5000 3650
Wire Wire Line
	5050 4000 5050 3800
Wire Wire Line
	5050 3800 5100 3800
Wire Wire Line
	5100 3800 5100 3650
Wire Wire Line
	5200 4000 5200 3650
Wire Wire Line
	5350 3800 5300 3800
Wire Wire Line
	5300 3800 5300 3650
Wire Wire Line
	5350 3800 5350 4000
Wire Wire Line
	5700 4350 5700 3750
Wire Wire Line
	5700 3750 5400 3750
Wire Wire Line
	5400 3750 5400 3650
Wire Wire Line
	5000 3750 4700 3750
Wire Wire Line
	4700 3750 4700 4350
Wire Wire Line
	4700 4500 4650 4500
Wire Wire Line
	4650 4500 4650 3700
Wire Wire Line
	4650 3700 4900 3700
Wire Wire Line
	4700 4650 4600 4650
Wire Wire Line
	4600 4650 4600 3650
Wire Wire Line
	4600 3650 4800 3650
Wire Wire Line
	5500 3650 5500 3700
Wire Wire Line
	5500 3700 5750 3700
Wire Wire Line
	5750 3700 5750 4500
Wire Wire Line
	5750 4500 5700 4500
Wire Wire Line
	5700 4650 5800 4650
Wire Wire Line
	5800 4650 5800 3650
Wire Wire Line
	5800 3650 5600 3650
Wire Wire Line
	4800 3150 4800 3100
Wire Wire Line
	4800 3100 5900 3100
Wire Wire Line
	5600 3100 5600 3150
Wire Wire Line
	4900 3150 4900 3100
Connection ~ 4900 3100
Wire Wire Line
	5000 3100 5000 3150
Connection ~ 5000 3100
Wire Wire Line
	5100 3150 5100 3100
Connection ~ 5100 3100
Wire Wire Line
	5200 3100 5200 3150
Connection ~ 5200 3100
Wire Wire Line
	5300 3150 5300 3100
Connection ~ 5300 3100
Wire Wire Line
	5400 3150 5400 3100
Connection ~ 5400 3100
Wire Wire Line
	5500 3150 5500 3100
Connection ~ 5500 3100
Wire Wire Line
	5900 3100 5900 5250
Connection ~ 5600 3100
Wire Wire Line
	5250 4750 4500 4750
Text GLabel 5900 5250 2    60   Input ~ 0
Negative
$Comp
L Rotary_Switch_12pos SW3
U 1 1 5B377AD1
P 7050 4500
F 0 "SW3" H 7050 4600 60  0000 C CNN
F 1 "Rotary_Switch_12pos" H 7050 4150 60  0000 C CNN
F 2 "myfootprint:Rotary_SW_noname" H 6000 4900 60  0001 C CNN
F 3 "" H 6000 4900 60  0001 C CNN
	1    7050 4500
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X09 J5
U 1 1 5B377AD7
P 7050 3400
F 0 "J5" H 7050 3900 50  0000 C CNN
F 1 "CONN_02X09" V 7050 3400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09_Pitch2.54mm" H 7050 2200 50  0001 C CNN
F 3 "" H 7050 2200 50  0001 C CNN
F 4 "FHDS20G11/RH" H 7050 3400 60  0001 C CNN "partno"
	1    7050 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6750 3700 6750 3650
Wire Wire Line
	6850 3750 6850 3650
Wire Wire Line
	6900 4000 6900 3800
Wire Wire Line
	6900 3800 6950 3800
Wire Wire Line
	6950 3800 6950 3650
Wire Wire Line
	7050 4000 7050 3650
Wire Wire Line
	7200 3800 7150 3800
Wire Wire Line
	7150 3800 7150 3650
Wire Wire Line
	7200 3800 7200 4000
Wire Wire Line
	7550 4350 7550 3750
Wire Wire Line
	7550 3750 7250 3750
Wire Wire Line
	7250 3750 7250 3650
Wire Wire Line
	6850 3750 6550 3750
Wire Wire Line
	6550 3750 6550 4350
Wire Wire Line
	6550 4500 6500 4500
Wire Wire Line
	6500 4500 6500 3700
Wire Wire Line
	6500 3700 6750 3700
Wire Wire Line
	6550 4650 6450 4650
Wire Wire Line
	6450 4650 6450 3650
Wire Wire Line
	6450 3650 6650 3650
Wire Wire Line
	7350 3650 7350 3700
Wire Wire Line
	7350 3700 7600 3700
Wire Wire Line
	7600 3700 7600 4500
Wire Wire Line
	7600 4500 7550 4500
Wire Wire Line
	7550 4650 7650 4650
Wire Wire Line
	7650 4650 7650 3650
Wire Wire Line
	7650 3650 7450 3650
Wire Wire Line
	6650 3150 6650 3100
Wire Wire Line
	6650 3100 7750 3100
Wire Wire Line
	7450 3100 7450 3150
Wire Wire Line
	6750 3150 6750 3100
Connection ~ 6750 3100
Wire Wire Line
	6850 3100 6850 3150
Connection ~ 6850 3100
Wire Wire Line
	6950 3150 6950 3100
Connection ~ 6950 3100
Wire Wire Line
	7050 3100 7050 3150
Connection ~ 7050 3100
Wire Wire Line
	7150 3150 7150 3100
Connection ~ 7150 3100
Wire Wire Line
	7250 3150 7250 3100
Connection ~ 7250 3100
Wire Wire Line
	7350 3150 7350 3100
Connection ~ 7350 3100
Wire Wire Line
	7750 3100 7750 5250
Connection ~ 7450 3100
Wire Wire Line
	7100 4750 6350 4750
Text GLabel 7750 5250 2    60   Input ~ 0
Negative
$Comp
L CONN_01X02 J1
U 1 1 5B377D2E
P 2000 3700
F 0 "J1" H 2000 3850 50  0000 C CNN
F 1 "CONN_01X02" V 2100 3700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02_Pitch2.54mm" H 2000 3700 50  0001 C CNN
F 3 "" H 2000 3700 50  0001 C CNN
F 4 "PHSR40G10" H 2000 3700 60  0001 C CNN "partno"
	1    2000 3700
	1    0    0    -1  
$EndComp
Text GLabel 1600 3750 0    60   Input ~ 0
Negative
Wire Wire Line
	1600 3750 1800 3750
$Comp
L Rotary_Switch_12pos SW4
U 1 1 5B37985C
P 9100 4500
F 0 "SW4" H 9100 4600 60  0000 C CNN
F 1 "Rotary_Switch_12pos" H 9100 4150 60  0000 C CNN
F 2 "myfootprint:Rotary_SW_noname" H 8050 4900 60  0001 C CNN
F 3 "" H 8050 4900 60  0001 C CNN
	1    9100 4500
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X09 J6
U 1 1 5B379862
P 9100 3400
F 0 "J6" H 9100 3900 50  0000 C CNN
F 1 "CONN_02X09" V 9100 3400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x09_Pitch2.54mm" H 9100 2200 50  0001 C CNN
F 3 "" H 9100 2200 50  0001 C CNN
F 4 "FHDS20G11/RH" H 9100 3400 60  0001 C CNN "partno"
	1    9100 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8800 3700 8800 3650
Wire Wire Line
	8900 3750 8900 3650
Wire Wire Line
	8950 4000 8950 3800
Wire Wire Line
	8950 3800 9000 3800
Wire Wire Line
	9000 3800 9000 3650
Wire Wire Line
	9100 4000 9100 3650
Wire Wire Line
	9250 3800 9200 3800
Wire Wire Line
	9200 3800 9200 3650
Wire Wire Line
	9250 3800 9250 4000
Wire Wire Line
	9600 4350 9600 3750
Wire Wire Line
	9600 3750 9300 3750
Wire Wire Line
	9300 3750 9300 3650
Wire Wire Line
	8900 3750 8600 3750
Wire Wire Line
	8600 3750 8600 4350
Wire Wire Line
	8600 4500 8550 4500
Wire Wire Line
	8550 4500 8550 3700
Wire Wire Line
	8550 3700 8800 3700
Wire Wire Line
	8600 4650 8500 4650
Wire Wire Line
	8500 4650 8500 3650
Wire Wire Line
	8500 3650 8700 3650
Wire Wire Line
	9400 3650 9400 3700
Wire Wire Line
	9400 3700 9650 3700
Wire Wire Line
	9650 3700 9650 4500
Wire Wire Line
	9650 4500 9600 4500
Wire Wire Line
	9600 4650 9700 4650
Wire Wire Line
	9700 4650 9700 3650
Wire Wire Line
	9700 3650 9500 3650
Wire Wire Line
	8700 3150 8700 3100
Wire Wire Line
	8700 3100 9800 3100
Wire Wire Line
	9500 3100 9500 3150
Wire Wire Line
	8800 3150 8800 3100
Connection ~ 8800 3100
Wire Wire Line
	8900 3100 8900 3150
Connection ~ 8900 3100
Wire Wire Line
	9000 3150 9000 3100
Connection ~ 9000 3100
Wire Wire Line
	9100 3100 9100 3150
Connection ~ 9100 3100
Wire Wire Line
	9200 3150 9200 3100
Connection ~ 9200 3100
Wire Wire Line
	9300 3150 9300 3100
Connection ~ 9300 3100
Wire Wire Line
	9400 3150 9400 3100
Connection ~ 9400 3100
Wire Wire Line
	9800 3100 9800 5250
Connection ~ 9500 3100
Wire Wire Line
	9150 4750 8400 4750
Text GLabel 9800 5250 2    60   Input ~ 0
Negative
Wire Wire Line
	1700 3650 1800 3650
Wire Wire Line
	1700 3600 1700 3650
Text GLabel 2650 4750 0    60   Input ~ 0
Positive
Text GLabel 4500 4750 0    60   Input ~ 0
Positive
Text GLabel 6350 4750 0    60   Input ~ 0
Positive
Text GLabel 8400 4750 0    60   Input ~ 0
Positive
Text Notes 2750 5350 0    60   ~ 0
0=OFF\n(Turn off the switch when\nthe decade is not desired)
Text Label 2750 4600 0    60   ~ 0
sw1-1
Text Label 2800 4450 0    60   ~ 0
sw1-2
Text Label 2850 4250 0    60   ~ 0
sw1-3
Text Label 3200 3900 0    60   ~ 0
sw1-4
Text Label 3350 4000 0    60   ~ 0
sw1-5
Text Label 3500 3900 0    60   ~ 0
sw1-6
Text Label 3850 3900 0    60   ~ 0
sw1-7
Text Label 3900 4000 0    60   ~ 0
sw1-8
Text Label 3950 4100 0    60   ~ 0
sw1-9
Text Label 4600 4600 0    60   ~ 0
sw2-1
Text Label 4650 4450 0    60   ~ 0
sw2-2
Text Label 4700 4250 0    60   ~ 0
sw2-3
Text Label 5050 4000 0    60   ~ 0
sw2-4
Text Label 5200 3900 0    60   ~ 0
sw2-5
Text Label 5300 3800 0    60   ~ 0
sw2-6
Text Label 5700 3850 0    60   ~ 0
sw2-7
Text Label 5750 3950 0    60   ~ 0
sw2-8
Text Label 5800 4050 0    60   ~ 0
sw2-9
Text Label 6450 4600 0    60   ~ 0
sw3-1
Text Label 6500 4450 0    60   ~ 0
sw3-2
Text Label 6550 4300 0    60   ~ 0
sw3-3
Text Label 6900 4000 0    60   ~ 0
sw3-4
Text Label 7050 3900 0    60   ~ 0
sw3-5
Text Label 7200 3800 0    60   ~ 0
sw3-6
Text Label 7550 3800 0    60   ~ 0
sw3-7
Text Label 7600 3900 0    60   ~ 0
sw3-8
Text Label 7650 4000 0    60   ~ 0
sw3-9
Text Label 8500 4600 0    60   ~ 0
sw4-1
Text Label 8550 4450 0    60   ~ 0
sw4-2
Text Label 8600 4300 0    60   ~ 0
sw4-3
Text Label 8950 4000 0    60   ~ 0
sw4-4
Text Label 9100 3900 0    60   ~ 0
sw4-5
Text Label 9250 3800 0    60   ~ 0
sw4-6
Text Label 9600 3800 0    60   ~ 0
sw4-7
Text Label 9650 3900 0    60   ~ 0
sw4-8
Text Label 9700 4000 0    60   ~ 0
sw4-9
$Comp
L CONN_01X02 J2
U 1 1 5B3DEDE1
P 2000 4000
F 0 "J2" H 2000 4150 50  0000 C CNN
F 1 "CONN_01X02" V 2100 4000 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 2000 4000 50  0001 C CNN
F 3 "" H 2000 4000 50  0001 C CNN
F 4 "DG126-5.0-02P-14-19AH" H 2000 4000 60  0001 C CNN "partno"
	1    2000 4000
	1    0    0    -1  
$EndComp
Connection ~ 1750 3650
Wire Wire Line
	1750 3950 1800 3950
Wire Wire Line
	1700 4050 1800 4050
Connection ~ 1700 3750
NoConn ~ 1750 3750
Wire Wire Line
	1750 3650 1750 3950
Wire Wire Line
	1700 3750 1700 4050
$EndSCHEMATC
